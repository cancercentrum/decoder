# Platform

|field    |value                        |
|:--------|:----------------------------|
|version  |R version 3.6.3 (2020-02-29) |
|os       |Windows 10 x64               |
|system   |x86_64, mingw32              |
|ui       |RStudio                      |
|language |(EN)                         |
|collate  |Swedish_Sweden.1252          |
|ctype    |Swedish_Sweden.1252          |
|tz       |Europe/Berlin                |
|date     |2020-04-22                   |

# Dependencies

|package |old   |new   |<U+0394>  |
|:-------|:-----|:-----|:--|
|decoder |1.2.1 |1.2.2 |*  |

# Revdeps

