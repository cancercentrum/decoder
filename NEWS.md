# decoder 1.2.2

* Include icd10cm
* Renamed icd10 to icd10se


# decoder 1.2

* Export keyvalue objects included in the package.
* Added a `NEWS.md` file to track changes to the package.
