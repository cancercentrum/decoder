Dear CRAN!

I hereby submit an updated version of ny decoder package.
It contains bug fixes and some minor updates.
It does effect the package 'incadata' (reversed dependency).
I will update that package to accordingly.

BR
Erik B
